<?php

namespace App\Entity;

use App\Repository\SaisonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaisonRepository::class)
 */
class Saison
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Mois::class, mappedBy="saison")
     */
    private $mois;

    public function __construct()
    {
        $this->mois = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Mois[]
     */
    public function getMois(): Collection
    {
        return $this->mois;
    }

    public function addMoi(Mois $moi): self
    {
        if (!$this->mois->contains($moi)) {
            $this->mois[] = $moi;
            $moi->setSaison($this);
        }

        return $this;
    }

    public function removeMoi(Mois $moi): self
    {
        if ($this->mois->removeElement($moi)) {
            // set the owning side to null (unless already changed)
            if ($moi->getSaison() === $this) {
                $moi->setSaison(null);
            }
        }

        return $this;
    }
}
