<?php

namespace App\Entity;

use App\Repository\LegumeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LegumeRepository::class)
 */
class Legume
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity=Mois::class, inversedBy="legumes")
     */
    private $mois;

    public function __construct()
    {
        $this->mois = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Mois[]
     */
    public function getMois(): Collection
    {
        return $this->mois;
    }

    public function addMoi(Mois $moi): self
    {
        if (!$this->mois->contains($moi)) {
            $this->mois[] = $moi;
        }

        return $this;
    }

    public function removeMoi(Mois $moi): self
    {
        $this->mois->removeElement($moi);

        return $this;
    }
}
