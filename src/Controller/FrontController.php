<?php

namespace App\Controller;

use App\Entity\Legume;
use App\Repository\LegumeRepository;
use App\Repository\MoisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FrontController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(){
        return $this->render("index.html.twig");
    }   
}