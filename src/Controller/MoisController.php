<?php

namespace App\Controller;

use App\Entity\Mois;
use App\Repository\MoisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mois")
 */
class MoisController extends AbstractController
{
    /**
     * @Route("/", name="mois_index", methods={"GET"})
     */
    public function index(MoisRepository $moisRepository): Response
    {
        return $this->render('mois/index.html.twig', [
            'mois' => $moisRepository->findAll(),
        ]);
    }

   

    /**
     * @Route("/{id}", name="mois_show", methods={"GET"})
     */
    public function show(Mois $mois): Response
    {
        return $this->render('mois/show.html.twig', [
            'mois' => $mois,
        ]);
    }

}
